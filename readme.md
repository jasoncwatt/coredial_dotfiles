**Fork this repo and make it your own!**

# How to Use

## New dev boxes

```sh
. setup_box.sh
```

this must be run like this as the script refreshed the .profile load for the terminal

## Manual Installs

### Setup and Dotfile backup

```sh
make
```

Run make from the base directory of this repo. This will ask for information and 
locations that will be set in the bashrc file and env vars. it will then backup
listed dotfiles and symlink the dotfiles in this repo. you can then commit and
push to a forked version of this repo to backup any dotfiles you wish.

### Installs

```sh
 make installs
```

This will install the basics, scripts, programs/libs needed. Also creates
a new SSH key (if not already created) and cats to the screen for easy copy into
Github and Gerrit, then run the following:


#### Applications

```sh
 cd installs/
 make apps
```

Check installs/Makefile for a which apps are available for installation.
Will install Chrome, Phpstorm, Pycharm, Hipchat, and sublime 

#### Dev

```sh
 cd installs/
 make dev
```

Running make dev from installs will create your sourcesDir folder if it does 
not exist and clone common git repos (chef, voiceaxis, etc..) then vagrant
up chef, web, api_web, app_mq1, sbc and agents

It also adds the sync/ dir which contains sync-branch.sh, used for updating 
all broadworks repos needed to a specified release.

