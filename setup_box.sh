#!/usr/bin/env bash

make ;
. ~/.profile
cd installs/
make video
cd ../
make install
cd installs/
make apps
ifconfig
# sudo ifconfig eno down
# sudo ip link set eno name eth0
# sudo ifconfig eth0 up
echo "About to bring the env up, make sure eth0 is named above. If not stop now and fix with commands in comments"
read -p "press [ENTER] to continue" ent ;
make dev/envup
cd ../